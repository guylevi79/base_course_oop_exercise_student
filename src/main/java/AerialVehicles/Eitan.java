package AerialVehicles;

import AerialVehicles.Haron;
import Capabilities.AttackCapability;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;

public class Eitan extends Haron {

  public Eitan(
      Coordinates motherBase,
      int status,
      int hoursAfterRepair,
      AttackCapability attackCapability,
      IntelligenceCapability intelligenceCapability) {
    super(motherBase, status, hoursAfterRepair);
    this.getCapabilities().add(attackCapability);
    this.getCapabilities().add(intelligenceCapability);
  }
}
