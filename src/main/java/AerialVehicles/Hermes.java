package AerialVehicles;

import Entities.Coordinates;

public abstract class Hermes extends Rpv {
  public Hermes(Coordinates motherBase, int status, int hoursAfterRepair) {
    super(motherBase, 160, status, hoursAfterRepair);
  }
}
