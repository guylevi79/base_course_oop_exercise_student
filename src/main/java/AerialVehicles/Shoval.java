package AerialVehicles;

import AerialVehicles.Haron;
import Capabilities.AttackCapability;
import Capabilities.BdaCapability;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;

public class Shoval extends Haron {
  public Shoval(
      Coordinates motherBase,
      int status,
      int hoursAfterRepair,
      AttackCapability attackCapability,
      IntelligenceCapability intelligenceCapability,
      BdaCapability bdaCapability) {
    super(motherBase, status, hoursAfterRepair);
    this.getCapabilities().add(attackCapability);
    this.getCapabilities().add(intelligenceCapability);
    this.getCapabilities().add(bdaCapability);
  }
}
