package AerialVehicles;

import Capabilities.Capability;
import Entities.Coordinates;


import java.util.ArrayList;
import java.util.List;

public abstract class AerialVehicle {

  private int hoursAfterRepair;
  private int flightStatus;
  private Coordinates motherBase;
  private int maxHoursAfterRepair;
  private List<Capability>capabilities;

  public AerialVehicle (Coordinates motherBase, int maxHoursAfterRepair, int status, int hoursAfterRepair){
    this.setFlightStatus(status);
    this.setHoursAfterRepair(hoursAfterRepair);
    this.setMotherBase(motherBase);
    this.setMaxHoursAfterRepair(maxHoursAfterRepair);
    this.setCapabilities(new ArrayList<>());
  };


  public void flyTo(Coordinates destination) {
    if (this.getFlightStatus() == 1 || this.getFlightStatus() == 2 ) {
      System.out.println(" Flying to:" + destination.toString());
      this.setFlightStatus(2);
    } else if (this.getFlightStatus() == 0) {
      System.out.println("Aerial Vehicle isn't ready to fly");
    }
  };

  public void land(Coordinates destination) {
    if (this.getFlightStatus() == 2) {
      System.out.println("Landing on: " + destination);
      this.check();
    } else {
      System.out.println("Aerial Vehicle is not in the air");
    }
  };

  public void check() {
    if (this.getHoursAfterRepair() >= this.getMaxHoursAfterRepair()){
      System.out.println("aerial vehicle need repair");
      this.setFlightStatus(0);
      this.repair();
    }
    else{
      this.setFlightStatus(1);
    }
  };

  private void repair() {
    this.setHoursAfterRepair(0);
    System.out.println("fixed");
    this.setFlightStatus(1);
  };

  private void addCapability (Capability capability){
    this.getCapabilities().add(capability);
  }

  public int getHoursAfterRepair() {
    return hoursAfterRepair;
  }

  public void setHoursAfterRepair(int hoursAfterRepair) {
    this.hoursAfterRepair = hoursAfterRepair;
  }

  public int getFlightStatus() {
    return flightStatus;
  }

  public List<Capability> getCapabilities() {
    return capabilities;
  }

  public void setCapabilities(List<Capability> capabilities) {
    this.capabilities = capabilities;
  }

  public void setFlightStatus(int flightStatus) {
    this.flightStatus = flightStatus;
  }

  public Coordinates getMotherBase() {
    return motherBase;
  }

  public void setMotherBase(Coordinates motherBase) {
    this.motherBase = motherBase;
  }

  public int getMaxHoursAfterRepair() {
    return maxHoursAfterRepair;
  }

  public void setMaxHoursAfterRepair(int maxHoursAfterRepair) {
    this.maxHoursAfterRepair = maxHoursAfterRepair;
  }
}
