package AerialVehicles;

import Entities.Coordinates;

public abstract class Fighter extends AerialVehicle {

    public Fighter(Coordinates motherBase, int status, int hoursAfterRepair) {
        super(motherBase, 250, status, hoursAfterRepair);
    }
}
