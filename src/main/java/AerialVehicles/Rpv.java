package AerialVehicles;

import Entities.Coordinates;

public abstract class Rpv extends AerialVehicle {

  public Rpv(Coordinates motherBase, int maxHoursAfterRepair, int status, int hoursAfterRepair) {
    super(motherBase, maxHoursAfterRepair, status, hoursAfterRepair);
  }

  public String hoverOverLocation(Coordinates destination) {
    this.setFlightStatus(2);
    String msg = "Hovering over " + destination;
    System.out.println(msg);

    return msg;
  }
}
