package AerialVehicles;

import AerialVehicles.Fighter;
import Capabilities.AttackCapability;
import Capabilities.BdaCapability;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;

public class F16 extends Fighter {
  public F16(
      Coordinates motherBase,
      int status,
      int hoursAfterRepair,
      AttackCapability attackCapability,
      BdaCapability bdaCapability) {
    super(motherBase, status, hoursAfterRepair);
    this.getCapabilities().add(attackCapability);
    this.getCapabilities().add(bdaCapability);
  }
}
