package AerialVehicles;

import AerialVehicles.Fighter;
import Capabilities.AttackCapability;
import Capabilities.Capability;
import Capabilities.IntelligenceCapability;
import Entities.Coordinates;

public class F15 extends Fighter {
  public F15(
      Coordinates motherBase,
      int status,
      int hoursAfterRepair,
      AttackCapability attackCapability,
      IntelligenceCapability intelligenceCapability) {
    super(motherBase, status, hoursAfterRepair);
    this.getCapabilities().add(attackCapability);
    this.getCapabilities().add(intelligenceCapability);
  }
}
