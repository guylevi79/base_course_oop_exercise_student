package Missions;

import AerialVehicles.AerialVehicle;
import Entities.Coordinates;

public abstract class Mission {

  private Coordinates destination;
  private String pilotName;
  private AerialVehicle aerialVehicle;

    public Mission(Coordinates destination, String pilotName, AerialVehicle aerialVehicle) {
        this.setDestination(destination);
        this.setPilotName(pilotName);
        this.setAerialVehicle(aerialVehicle);
    }

    public void begin() {
    System.out.println("Beginning Mission!");
    this.getAerialVehicle().flyTo(this.getDestination());
  };

  public void cancel() {
    System.out.println("Abort Mission!");
    this.getAerialVehicle().land(this.getAerialVehicle().getMotherBase());
  };

  public void finish() throws AerialVehicleNotCompatibleException {
    this.executeMission();
    this.getAerialVehicle().land(this.getAerialVehicle().getMotherBase());
    System.out.println("Finish mission!");
  };

  public String getPilotName() {
    return pilotName;
  }

  public void setPilotName(String pilotName) {
    this.pilotName = pilotName;
  }

  abstract void executeMission() throws AerialVehicleNotCompatibleException;

  public AerialVehicle getAerialVehicle() {
    return aerialVehicle;
  }

  public void setAerialVehicle(AerialVehicle aerialVehicle) {
    this.aerialVehicle = aerialVehicle;
  }

  public Coordinates getDestination() {
    return destination;
  }

  public void setDestination(Coordinates destination) {
    this.destination = destination;
  }
}
